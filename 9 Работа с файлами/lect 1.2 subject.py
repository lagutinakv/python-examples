# Какой предмет любит самое большое количество школьников
import os

with open(os.path.join('data', 'data_school_subject.txt'), 'r') as input_file:
    lines = input_file.readlines()
subjects = lines[2::3]
subjects = list(map(str.strip, subjects))
subject_counters = {}
for subj in subjects:
    if subj in subject_counters:
        subject_counters[subj] += 1
    else:
        subject_counters[subj] = 1
most_popular = max(subject_counters, key=subject_counters.get)
print(f'Самый популярный предмет - {most_popular} у {subject_counters[most_popular]} школьников')
