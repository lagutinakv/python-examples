import math

print(math.ceil(5.1)) # Округление вверх
num = -8.2
low_num = math.floor(num) # Округление вниз
print(low_num)
print(math.fabs(low_num)) # Модуль числа
print(math.isfinite(5.5)) # Является ли числом
print(math.trunc(num)) # Целая часть
print(math.pow(num, 0)) # Возведение в степень
print(math.sqrt(math.fabs(low_num))) # Корень из числа
print(math.cos(math.pi)) # Тригонометрические функции
