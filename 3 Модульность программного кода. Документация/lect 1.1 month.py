def get_day_count(month_number):
    if month_number > 12 or month_number < 1: # Если номер месяца не корректен
        return -1
    if month_number == 1 or month_number == 3 or month_number == 5 or month_number == 7 or \
            month_number == 8 or month_number == 10 or month_number == 12:
        # Если месяц - январь, март, май, июль, август, октябрь или декабрь
        return 31
    if month_number == 2:  # Если месяц - февраль
        return 28
    return 30  # Остальные месяцы


month = int(input()) # Вводим номер месяца
day_count = get_day_count(month)
if day_count == -1:
    print('Номер месяца должен быть числом от 1 до 12')
else:
    print(day_count)
