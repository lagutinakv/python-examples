# В словаре хранится список участников конкурса и их баллов за конкурс:
#        Иванов 20
#        Сидоров 68
#        Петров 26
#        Смирнов 68
# Выведите на экран список участников, балл которых выше среднего.
# Выведите на экран минимальный, максимальный и средний баллы участников.
# Минимальный и максимальный баллы выведите вместе с фамилиями всех участников, которые их получили.
from functools import reduce

members = {'Иванов': 20, 'Сидоров': 68, 'Петров': 26, 'Смирнов': 68} # Заводим словарь
mean_score = sum(members.values()) / len(members) # Вычисляем среднй балл

members_high_score = {} # Пустой словарь для оценок выше среднего
for key, value in members.items(): # Перебираем все пары в словаре
    if value > mean_score: # Если оценка выше среднего, добавляем пару в новый словарь
        members_high_score[key] = value
print(members_high_score)
# Альтернативно: фильтрация в функциональном стиле
members_high_score = filter(lambda elem: elem[1] > mean_score, members.items())
print(dict(members_high_score))

print('Средний балл:', mean_score)

min_score = 100 # Минимальная оценка
min_score_members = [] # Список участников с минимальной оценкой
for key, value in members.items(): # Перебираем все пары в словаре
    if value < min_score: # Если найдена меньшая оценка
        min_score = value # заоминаем её
        min_score_members = [key] # и добавляем фамилию в список
    elif value == min_score: # если найден ещё один участник с минимальной оценкой
        min_score_members.append(key) # добавляем участника в имеющийся список
print('Минимальный балл', min_score, 'получили:', ', '.join(min_score_members))
# Максимальный балл можно искать таким же способом

# Альтернативный вариант поиска в функциональном стиле:
max_score = reduce(lambda res, member: max(res, member[1]), members.items(), 0) # Ищем максимальный балл
max_score_members = dict(filter(lambda member: member[1] == max_score, members.items())) # Ищем всех, у кого балл равен максимальному
print('Максимальный балл', max_score, 'получили:', ', '.join(max_score_members))
