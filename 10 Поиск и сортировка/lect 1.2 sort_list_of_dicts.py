from operator import itemgetter

users = [{'username': 'Alice', 'joined': 2020, 'age': 23},
         {'username': 'Bob', 'joined': 2018, 'age': 19},
         {'username': 'anna', 'joined': 2020, 'age': 31},
         {'username': 'Ivan', 'joined': 2022, 'age': 23}]
users_sorted_default = sorted(users, key=itemgetter('username'))
print(users_sorted_default)
users_sorted_caseless = sorted(users, key=lambda u: u['username'].lower())
print(users_sorted_caseless)
