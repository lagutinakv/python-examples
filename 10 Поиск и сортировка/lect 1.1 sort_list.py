users = ['username', 'Alice', 'joined', 'age', 'Bob','Anna', 'Ivan']
users_sorted_default = sorted(users)
print(users_sorted_default)
users_sorted_caseless = sorted(users, key=str.lower)
print(users_sorted_caseless)
print('Исходный массив: ', users)
# Аналогично
users.sort(key=str.lower)
print(users)
