from functools import reduce

arr = [4, -5, 6, 15, 7, -5, 11, 0]
filtered_arr = filter(lambda num: num % 5 != 0, arr)
print(list(filtered_arr))

counter = reduce(lambda res, num: res + 1 if num % 5 == 0 and num >= 0 else res, arr, 0)
print(counter)
